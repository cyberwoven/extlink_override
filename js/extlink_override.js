(function (Drupal, $, once, drupalSettings) {
  Drupal.extlink.popupClickHandler = function (e) {

    // Stop the click event
    e.preventDefault();
    e.stopPropagation();

    $.fancybox.open({
      src: '#link-dialog-block',
      type: 'inline',
    });

    let dialogOK = $('#link-dialog-block .link-dialog-ok');
    let target = $(e.currentTarget);

    dialogOK.attr('data-new', (target.attr('target') === '_blank' || target.attr('rel') === 'external') ? '_blank' : '_self');
    dialogOK.attr('data-url', target.attr('href'));

    return false;
  };

  $(once('link-dialog-ok', '#link-dialog-block .link-dialog-ok')).on('click', function (event) {
    event.preventDefault();

    let dialogOK = $('#link-dialog-block .link-dialog-ok');
    let url = dialogOK.attr('data-url');

    if (dialogOK.attr('data-new') === '_blank') {
      $.fancybox.close();
      window.open(url, '_blank');
    } else {
      window.location.href = url;
    }
  });

  $(once('link-dialog-close', '#link-dialog-block .link-dialog-close')).on('click', function (event) {
    event.preventDefault();
    $.fancybox.close();
  });

})(Drupal, jQuery, once, drupalSettings);

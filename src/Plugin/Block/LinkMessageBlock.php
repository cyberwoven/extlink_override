<?php

/**
 * @file
 * Contains \Drupal\extlink_override\Plugin\Block\LinkMessageBlock.
 */

namespace Drupal\extlink_override\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'LinkMessage' block.
 *
 * @Block(
 *  id = "link_message",
 *  admin_label = @Translation("External Link message"),
 * )
 */
class LinkMessageBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::service('config.factory')->getEditable('extlink.settings');
    $module_config = \Drupal::service('config.factory')->getEditable('extlink_override.settings');

    $build = [];
    $block = [
      '#theme' => 'extlink_override',
      '#attributes' => [
        'class' => ['link-dialog'],
        'id' => 'link-dialog-block',
      ],
      '#title' => $module_config->get('title'),
      '#message' => $config->get('extlink_alert_text'),
      '#cache' => [
        'max-age' => Cache::PERMANENT, // TODO: link CacheTag to config message
      ],
    ];

    $build['link_message'] = $block;
    return $build;
  }

}
